# TP5-conception-logicielle

Bonjour, avec cette application, vous allez pouvoir détecter si un produit est vegan ou non à partir de son code-barre. 
## Installer les dépendances
Installer les dépendances nécessaires à exécuter le code, tapez cette ligne dans le terminal : 
```pip install -r requirements.txt```

## Lancer l'application
Taper dans votre terminal : 
```uvicorn main:app --reload```

## Cliquer sur le lien http de la sortie


1.  Ajouter à la fin de l'adresse http : /docs#

2.  Dans la page qui s'ouvre, cliquez sur le GET puis sur le *Try it out*

3.  Entrez dans id_produit, l'id du produit que vous souhaitez tester (*vous pouvez tester avec 3256540001305 qui est un pitch*)

4.  Vous visualiser si votre produit est vegan ou non, dans la partie *Response* -> *Code* -> *Response body*





