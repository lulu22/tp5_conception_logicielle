import requests
from fastapi import FastAPI

requete = requests.get("https://world.openfoodfacts.org/api/v0/product/3256540001305")
#ingredients = requete.json()["product"]["ingredients"]
#for dico in ingredients:
    #print((dico["text"], dico["vegan"]))
reponse = requete.json()

def isVegan(requestAsJson):
    ingredients = requestAsJson["product"]["ingredients"]
    for dico in ingredients :
        if 'vegan' in dico:
            if (dico["vegan"] == "no" or dico["vegan"] == "maybe"):
                return False
    return True

print(isVegan(reponse))


app = FastAPI(
    title="Prédire",
    description="Prédire si votre produit est vegan ou non",
    version="1.0.0")

@app.get("/id_produit")
async def vegan_or_not(id_produit):
    requete = requests.get("https://world.openfoodfacts.org/api/v0/product/{}".format(id_produit))
    output = isVegan(requete.json())
    return output